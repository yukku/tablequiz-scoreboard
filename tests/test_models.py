import math

import pytest
from django.db.models import Sum
from django.db.utils import IntegrityError
from scoreboard.models import Score
from tests.factories import AttendeeFactory, QuestionFactory, ScoreboardFactory, ScoreFactory


@pytest.mark.django_db(transaction=True)
def test_create_scoreboard():
    NUMBER_OF_ROUNDS = 5
    NUMBER_OF_QUESTIONS_PER_ROUND = 3

    attendee_0 = AttendeeFactory(name="John")
    attendee_1 = AttendeeFactory(name="Amy")
    attendee_2 = AttendeeFactory(name="Bob")

    questions = []
    for i in range(NUMBER_OF_ROUNDS * NUMBER_OF_QUESTIONS_PER_ROUND):
        question = QuestionFactory(
            question_number=i + 1, round_number=math.floor(i / NUMBER_OF_ROUNDS) + 1
        )
        questions.append(question)

    scoreboard = ScoreboardFactory()
    scoreboard.questions.set(questions)
    scoreboard.attendees.set([attendee_0, attendee_1, attendee_2])
    scoreboard.save()

    ScoreFactory(score=1, attendee=attendee_0, question=questions[0], scoreboard=scoreboard)
    ScoreFactory(score=0, attendee=attendee_0, question=questions[1], scoreboard=scoreboard)
    ScoreFactory(score=1, attendee=attendee_0, question=questions[2], scoreboard=scoreboard)

    ScoreFactory(score=1, attendee=attendee_1, question=questions[0], scoreboard=scoreboard)
    ScoreFactory(score=1, attendee=attendee_1, question=questions[1], scoreboard=scoreboard)
    ScoreFactory(score=1, attendee=attendee_1, question=questions[2], scoreboard=scoreboard)

    attendee_0_total = Score.objects.filter(attendee=attendee_0, scoreboard=scoreboard).aggregate(
        Sum("score")
    )["score__sum"]
    assert attendee_0_total == 2

    attendee_1_total = Score.objects.filter(attendee=attendee_1, scoreboard=scoreboard).aggregate(
        Sum("score")
    )["score__sum"]
    assert attendee_1_total == 3

    attendee_2_total = Score.objects.filter(attendee=attendee_2, scoreboard=scoreboard).aggregate(
        Sum("score")
    )["score__sum"]
    assert attendee_2_total == None


@pytest.mark.django_db(transaction=True)
def test_dublicate_score():
    attendee = AttendeeFactory(name="John")
    question = QuestionFactory(question_number=1, round_number=1)
    scoreboard = ScoreboardFactory()

    with pytest.raises(IntegrityError):
        ScoreFactory(score=1, attendee=attendee, question=question, scoreboard=scoreboard)
        ScoreFactory(score=1, attendee=attendee, question=question, scoreboard=scoreboard)
