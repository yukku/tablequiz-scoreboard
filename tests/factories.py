import factory

from scoreboard.models import Attendee, Question, Score, Scoreboard


class ScoreboardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Scoreboard


class QuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Question


class AttendeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Attendee


class ScoreFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Score
