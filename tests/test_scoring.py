import json
from collections import OrderedDict

import pytest
from scoreboard.exceptions import InvalidScoreError, InvalidAttendeeError
from scoreboard.scoring import create_scoreboard, create_scores_per_round, get_total_scores


@pytest.mark.django_db(transaction=True)
def test_create_scoreboard():
    number_of_rounds = 2
    number_of_questions_per_round = 3
    attendee_names = ["John", "Amy", "Bob"]

    scoreboard = create_scoreboard(
        number_of_rounds=number_of_rounds,
        number_of_questions_per_round=number_of_questions_per_round,
        attendee_names=attendee_names,
    )

    assert scoreboard.questions.count() == 6
    assert scoreboard.attendees.count() == 3


@pytest.mark.django_db(transaction=True)
def test_invalid_attendee_names():
    number_of_rounds = 2
    number_of_questions_per_round = 3
    attendee_names = ["John", "Amy", "Amy"]

    with pytest.raises(InvalidAttendeeError):
        scoreboard = create_scoreboard(
            number_of_rounds=number_of_rounds,
            number_of_questions_per_round=number_of_questions_per_round,
            attendee_names=attendee_names,
        )


@pytest.mark.django_db(transaction=True)
def test_create_scores_per_round():
    number_of_rounds = 2
    number_of_questions_per_round = 3
    attendee_names = ["John", "Amy", "Bob"]

    scoreboard = create_scoreboard(
        number_of_rounds=number_of_rounds,
        number_of_questions_per_round=number_of_questions_per_round,
        attendee_names=attendee_names,
    )

    scores = {"John": [0, 1, 1], "Amy": [1, 1, 1], "Bob": [1, 0, 0]}

    create_scores_per_round(scores=scores, round_number=1, scoreboard_id=scoreboard.id)

    total_scores = get_total_scores(scoreboard_id=scoreboard.id)

    assert json.dumps(total_scores) == json.dumps(
        OrderedDict([("Amy", 3), ("John", 2), ("Bob", 1)])
    )


@pytest.mark.django_db(transaction=True)
def test_reporting_spurious_result():
    number_of_rounds = 2
    number_of_questions_per_round = 3
    attendee_names = ["John", "Amy", "Bob"]

    scoreboard = create_scoreboard(
        number_of_rounds=number_of_rounds,
        number_of_questions_per_round=number_of_questions_per_round,
        attendee_names=attendee_names,
    )

    scores = {"John": [0, 1, 1], "Amy": [1, 1, 1, 1], "Bob": [1, 0, 0]}

    with pytest.raises(InvalidScoreError):
        create_scores_per_round(scores=scores, round_number=1, scoreboard_id=scoreboard.id)
