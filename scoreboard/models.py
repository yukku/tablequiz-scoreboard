from django.db import models


class Scoreboard(models.Model):
    attendees = models.ManyToManyField("Attendee")
    questions = models.ManyToManyField(
        "Question",
    )

    def __str__(self):
        return f"<Scoreboard id={self.id}>"


class Attendee(models.Model):
    name = models.CharField(max_length=256)
    questions = models.ManyToManyField(
        "Question",
        through="Score",
        through_fields=("attendee", "question"),
    )

    def __str__(self):
        return f"<Attendee name={self.name}>"


class Question(models.Model):
    question_number = models.PositiveSmallIntegerField()
    round_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"<Question id={self.id}>"


class Answer(models.IntegerChoices):
    WRONG = 0, "Wrong"
    CORRECT = 1, "Correct"

    __empty__ = "(Unknown)"


class Score(models.Model):
    class Meta:
        unique_together = (
            "attendee",
            "question",
        )

    score = models.PositiveSmallIntegerField(choices=Answer.choices)
    scoreboard = models.ForeignKey("Scoreboard", on_delete=models.CASCADE)
    attendee = models.ForeignKey("Attendee", on_delete=models.CASCADE)
    question = models.ForeignKey(
        "Question",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"<Score attendee={self.attendee.name} score={self.score}>"
