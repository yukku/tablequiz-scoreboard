import math
from collections import OrderedDict

from django.db.models import Sum
from django.db.transaction import atomic
from django.db.utils import IntegrityError
from scoreboard.exceptions import InvalidScoreError, InvalidAttendeeError
from scoreboard.models import Answer, Attendee, Question, Score, Scoreboard


@atomic
def create_scoreboard(number_of_rounds, number_of_questions_per_round, attendee_names):
    """Create a scoreboard

    Args:
        number_of_rounds (int): Number of a round
        number_of_questions_per_round (int): Id of the scoreboard
        attendee_names (str): Name of attendee (Need to be unique per tablequiz)

    Returns:
        A instance of Scoreboard
    """
    assert number_of_rounds > 1
    assert number_of_questions_per_round > 1
    assert len(attendee_names) > 1

    if len(attendee_names) != len(set(attendee_names)):
        raise InvalidAttendeeError("There seems to be duplicated attendee names")

    attendees = [Attendee.objects.create(name=attendee_name) for attendee_name in attendee_names]

    questions = []
    for i in range(number_of_rounds * number_of_questions_per_round):
        question = Question.objects.create(
            question_number=i + 1, round_number=math.floor(i / number_of_questions_per_round) + 1
        )
        questions.append(question)

    scoreboard = Scoreboard.objects.create()
    scoreboard.questions.set(questions)
    scoreboard.attendees.set(attendees)
    scoreboard.save()

    return scoreboard


@atomic
def create_scores_per_round(scores, round_number, scoreboard_id):
    """Create and stores scores for each attendee

    Args:
        scores (dict of list):
            example:
            {
                "John": [0, 1, 1],
                "Amy": [1, 1, 1]
            }
        round_number (int): Number of a round
        scoreboard (int): Id of the scoreboard
    """

    scoreboard = Scoreboard.objects.get(id=scoreboard_id)
    questions = scoreboard.questions.filter(round_number=round_number)

    for attendee_name, scores in scores.items():
        attendee = scoreboard.attendees.get(name=attendee_name)
        _validate_scores_for_round(attendee, scores, round_number, scoreboard)

        for i, score in enumerate(scores):
            _create_score(score, attendee, questions[i], scoreboard)


def get_total_scores(scoreboard_id):
    """Returns the total scores for each attendee sorted by the score

    Args:
        scoreboard_id (int): Id of the scoreboard

    Returns:
        OrderedDict:
            example:
            OrderedDict([
                ('Amy', 3),
                ('John', 2),
                ('Bob', 1),
            ])

    """
    scoreboard = Scoreboard.objects.get(id=scoreboard_id)

    result = OrderedDict()
    for attendee in scoreboard.attendees.all():
        result[attendee.name] = Score.objects.filter(
            attendee=attendee, scoreboard=scoreboard
        ).aggregate(Sum("score"))["score__sum"]

    return OrderedDict(sorted(result.items(), key=lambda item: item[1], reverse=True))


def _create_score(score, attendee, question, scoreboard):
    try:
        return Score.objects.create(
            score=score, attendee=attendee, question=question, scoreboard=scoreboard
        )
    except IntegrityError:
        previous_score = Score.objects.get(
            attendee=attendee, question=question, scoreboard=scoreboard
        )
        raise InvalidScoreError(
            (
                f"The score has already been created for {attendee.name}. "
                f"Previously recorded score for question {question.question_number} in round {question.round_number} is {previous_score.score}. "
            )
        )


def _get_round_total(round_number, scoreboard):
    return scoreboard.questions.filter(round_number=round_number).count()


def _validate_scores_for_round(attendee, scores, round_number, scoreboard):
    round_total = _get_round_total(round_number, scoreboard)
    if len(scores) > round_total:
        raise InvalidScoreError(
            f"The reported score by {attendee.name} is greater than total round score of {round_total}."
        )
    if len(scores) < round_total:
        raise InvalidScoreError(
            f"The reported score by {attendee.name} is less than total round score of {round_total}."
        )
