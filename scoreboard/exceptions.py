class InvalidScoreError(Exception):
    pass


class InvalidAttendeeError(Exception):
    pass
