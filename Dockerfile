FROM python:3.9.2

COPY ./ /srv/app
WORKDIR /srv/app

RUN pip install -r requirements.txt
CMD python manage.py migrate && tail -f /dev/null

