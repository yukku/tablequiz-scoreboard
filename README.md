# Tablequiz scoreboard

Django backend library for creating scoreboards for tablequize. 


It's designed to store the following entities.

- Scoreboard
- Attendee
- Question
- Score

**1.** In order to start, Scoreboard will require the **number of rounds**, **number of questions per round** and the **list of attendees names**. 

```python
number_of_rounds=5
number_of_questions_per_round=3
attendee_names=["John", "Amy", "Bob"]

``` 

**2.** Scores are collected at the end of each round as the following.

```python
"""
score
0 = Wrong
1 = Correct
"""

scores={
    "John": [0, 1, 1],
    "Amy": [1, 1, 1],
    "Bob": [1, 0, 0],
}
round_number=2
scoreboard_id=1

``` 

**3.** The total scores for each atendee can be obtained by identifier for the scoreboard returned by initialisation step [1]. It will return the result sorted by the highest score.

```python
OrderedDict([
    ('Amy', 3),
    ('Bob', 1),
    ('John', 2)
])

``` 

The score history for scoreboards and each participant is maintained in SQLite database. It can be easily switched to alternative databases supported by Django. The app can be packaged and distributed by adapting [this tutorial](https://docs.djangoproject.com/en/3.2/intro/reusable-apps/).

The app has minimum implementation for storing basic scoreboard logic. It can be extended to render views either in html or REST APIs such as [DRF](https://www.django-rest-framework.org).


### Installation

```
docker-compose up
```


### Running tests

```
docker-compose exec scoreboard_app pytest
```
